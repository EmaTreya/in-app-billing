package com.santamaria.inappbilling_test;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.santamaria.inappbilling_test.billing.BillingManager;
import com.santamaria.inappbilling_test.billing.BillingProvider;

import java.util.List;

import static com.santamaria.inappbilling_test.billing.BillingConstants.MONTHLY_SUSCRIPTION;
import static com.santamaria.inappbilling_test.billing.BillingConstants.ONE_TIME_PURCHASE;
import static com.santamaria.inappbilling_test.billing.BillingManager.BILLING_MANAGER_NOT_INITIALIZED;

public class MainActivity extends AppCompatActivity implements BillingProvider {

    private Button buyOneTime;
    private Button suscription;
    private UpdateListener updateListener;

    private BillingManager mBillingManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buyOneTime = findViewById(R.id.buyOneTime);
        suscription = findViewById(R.id.suscription);
        updateListener = new UpdateListener(this);

        // Create and initialize BillingManager which talks to BillingLibrary
        mBillingManager = new BillingManager(this, updateListener);

        buyOneTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isPremiumPurchased()){
                    Toast.makeText(view.getContext(), "Already a Premium User", Toast.LENGTH_SHORT).show();
                } else if (mBillingManager != null
                        && mBillingManager.getBillingClientResponseCode()
                        > BILLING_MANAGER_NOT_INITIALIZED) {

                    getBillingManager().initiatePurchaseFlow(ONE_TIME_PURCHASE,
                            BillingClient.SkuType.INAPP);
                }
            }
        });

        suscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isMonthlySubscribed()){
                    Toast.makeText(view.getContext(), "Already subscripted", Toast.LENGTH_SHORT).show();
                } else if (mBillingManager != null
                        && mBillingManager.getBillingClientResponseCode()
                        > BILLING_MANAGER_NOT_INITIALIZED) {

                    getBillingManager().initiatePurchaseFlow(MONTHLY_SUSCRIPTION,
                            BillingClient.SkuType.SUBS);
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        // Note: We query purchases in onResume() to handle purchases completed while the activity
        // is inactive. For example, this can happen if the activity is destroyed during the
        // purchase flow. This ensures that when the activity is resumed it reflects the user's
        // current purchases.
        if (mBillingManager != null
                && mBillingManager.getBillingClientResponseCode() == BillingClient.BillingResponse.OK) {
            mBillingManager.queryPurchases();
        }

        if (isPremiumPurchased()){
            Toast.makeText(this, "I'M PREMIUM", Toast.LENGTH_SHORT).show();
        }

        if (isMonthlySubscribed()){
            Toast.makeText(this, "I'M SUBSCRIBED FOR 1 MONTH", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        if (mBillingManager != null){
            mBillingManager.destroy();
        }
        super.onDestroy();
    }

    @Override
    public BillingManager getBillingManager() {
        return mBillingManager;
    }

    @Override
    public boolean isPremiumPurchased() {
        return updateListener.isPremium();
    }

    @Override
    public boolean isMonthlySubscribed() {
        return updateListener.isMontlySuscriptionEnable();
    }

}
