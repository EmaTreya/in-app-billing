package com.santamaria.inappbilling_test;

import android.util.Log;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.santamaria.inappbilling_test.billing.BillingManager;

import java.util.List;

import static android.content.ContentValues.TAG;
import static com.santamaria.inappbilling_test.billing.BillingConstants.MONTHLY_SUSCRIPTION;
import static com.santamaria.inappbilling_test.billing.BillingConstants.ONE_TIME_PURCHASE;

/**
 * Created by Santamaria on 17/03/2018.
 */

public class UpdateListener implements BillingManager.BillingUpdatesListener {

    private MainActivity mActivity;

    public boolean isPremium() {
        return isPremium;
    }

    public boolean isMontlySuscriptionEnable() {
        return isMontlySuscription;
    }


    private boolean isPremium = false;
    private boolean isMontlySuscription = false;

    public UpdateListener(MainActivity mActivity) {
        this.mActivity = mActivity;
    }

    @Override
    public void onConsumeFinished(String token, @BillingClient.BillingResponse int result) {
        Log.d(TAG, "Consumption finished. Purchase token: " + token + ", result: " + result);


        // Note: We know this is the SKU_GAS, because it's the only one we consume, so we don't
        // check if token corresponding to the expected sku was consumed.
        // If you have more than one sku, you probably need to validate that the token matches
        // the SKU you expect.
        // It could be done by maintaining a map (updating it every time you call consumeAsync)
        // of all tokens into SKUs which were scheduled to be consumed and then looking through
        // it here to check which SKU corresponds to a consumed token.
        if (result == BillingClient.BillingResponse.OK) {
            // Successfully consumed, so we apply the effects of the item in our
            // game world's logic, which in our case means filling the gas tank a bit
            Log.d(TAG, "Consumption successful. Provisioning.");
          /*  mTank = mTank == TANK_MAX ? TANK_MAX : mTank + 1;
            saveData();
            mActivity.alert(R.string.alert_fill_gas, mTank);*/
        } else {
           // mActivity.alert(R.string.alert_error_consuming, result);
        }

        Log.d(TAG, "End consumption flow.");
    }

    @Override
    public void onPurchasesUpdated(List<Purchase> purchaseList) {

        isPremium = false;
        isMontlySuscription = false;

        Log.d(TAG, "onPurchasesUpdated");
        for (Purchase purchase : purchaseList) {
            //done to clear the sell on the cloud just for testing
            mActivity.getBillingManager().consumeAsync(purchase.getPurchaseToken());
        }

        for (Purchase purchase : purchaseList) {
            switch (purchase.getSku()) {
                case "android.test.purchased":
                    isPremium = true;
                    break;
                case ONE_TIME_PURCHASE:
                    isPremium = true;
                    break;
                case MONTHLY_SUSCRIPTION:
                    isMontlySuscription = true;
                    break;
             /*   case GoldMonthlyDelegate.SKU_ID:
                    mGoldMonthly = true;
                    break;*/
            }
        }
    }
}
